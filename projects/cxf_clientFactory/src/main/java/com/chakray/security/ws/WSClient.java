package com.chakray.security.ws;

import org.apache.cxf.binding.soap.saaj.SAAJStreamWriter;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.wss4j.common.util.XMLUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Iterator;
import java.util.Map;


public class WSClient {

    public static void main(String[] args) throws Exception {
        testVersionService();
    }

    static void testVersionService() throws Exception {
        ApplicationContext factory = new ClassPathXmlApplicationContext("client-context.xml");
        Client client = ClientProxy.getClient(factory.getBean("versionService"));
        client.invoke("getVersion", "");
        SAAJStreamWriter responseWriter = ((SAAJStreamWriter) client.getResponseContext().get("org.apache.cxf.staxutils.W3CDOMStreamWriter"));
        if (responseWriter!=null) System.out.println("Response :" + XMLUtils.prettyDocumentToString(responseWriter.getDocument()));

    }

    static void testEchoService() throws Exception {
        ApplicationContext factory = new ClassPathXmlApplicationContext("client-context.xml");
        Client client = ClientProxy.getClient(factory.getBean("echoService"));

        Object[] response = client.invoke("echoString", String.valueOf(System.currentTimeMillis()));
        for (
                int i = 0;
                i < response.length; i++)

        {
            System.out.println("response : " + response[i]);
        }

        Map<String, Object> responseContext = client.getResponseContext();

        /*BindingMessageInfo messageInfo = ((BindingMessageInfo) responseContext.get("org.apache.cxf.service.model.BindingMessageInfo"));
        ListIterator<MessagePartInfo> it = messageInfo.getMessageParts().listIterator();
        while(it.hasNext()){
            ((MessagePartInfo)it.next()).getMessageInfo().;
        }
        *//*
        Map<String, Object> requestContext = client.getRequestContext();
*/
        /*       for (int i = 0; i < response.length; i++) {
            System.out.println("response : " + response[i]);
        }
        Map<String, Object> responseContext = client.getResponseContext();
        SAAJStreamWriter responseWriter = ((SAAJStreamWriter) responseContext.get("org.apache.cxf.staxutils.W3CDOMStreamWriter"));
        Iterator<String> keys = responseContext.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            System.out.println("Key: " + key + " Value: " + responseContext.get(key));
        }
*/
        Iterator<String> keys = responseContext.keySet().iterator();
        while (keys.hasNext())

        {
            String key = keys.next();
            System.out.println("Key: " + key + " Value: " + responseContext.get(key));
        }
        System.out.println("Response :" + client.getResponseContext());
    }

}