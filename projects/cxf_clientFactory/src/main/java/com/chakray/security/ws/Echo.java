package com.chakray.security.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService (serviceName = "echo", name="echo", targetNamespace = "http://echo.services.core.carbon.wso2.org"
)
public interface Echo

{
    @WebMethod(action="echoString")

    String echoString(String string);
}