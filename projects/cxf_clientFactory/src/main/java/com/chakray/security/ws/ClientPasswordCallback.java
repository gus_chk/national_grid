    package com.chakray.security.ws;


    import org.apache.ws.security.WSPasswordCallback;

    import javax.security.auth.callback.Callback;
    import javax.security.auth.callback.CallbackHandler;
    import javax.security.auth.callback.UnsupportedCallbackException;
    import java.io.IOException;
    import java.util.HashMap;
    import java.util.Map;


    public class ClientPasswordCallback implements CallbackHandler
    {

        private Map<String, String> passwords = new HashMap<String, String>();

        public ClientPasswordCallback()
        {
            passwords.put("client", "password");
        }

        public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException
        {
            for (Callback callback : callbacks)
            {
                WSPasswordCallback passwordCallback = (WSPasswordCallback) callback;
    /*            System.out.println("Identifier: "+ passwordCallback.getIdentifier());
                System.out.println("getType: "+ passwordCallback.getType());
                System.out.println("getRequestData: "+ passwordCallback.getRequestData());
                System.out.println("getUsage: "+ passwordCallback.getUsage());*/

                String password = passwords.get(passwordCallback.getIdentifier());
                if (password != null)
                {
                    passwordCallback.setPassword(password);
    /*
                    System.out.println("getPassword: "+ passwordCallback.getPassword());
    */
                    return;
                }
            }
        }
    }