package com.chakray.security.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService (serviceName = "Version", name="Version", targetNamespace = "http://version.services.core.carbon.wso2.org"
)
public interface Version

{
    @WebMethod(action="getVersion")

    String getVersion();
}