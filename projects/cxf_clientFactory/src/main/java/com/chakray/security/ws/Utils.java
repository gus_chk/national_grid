package com.chakray.security.ws;

import org.apache.cxf.binding.soap.saaj.SAAJStreamWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.Map;

public class Utils {

    protected static String getPretty(Map<String, Object> responseContext) throws TransformerException {
        SAAJStreamWriter responseWriter = ((SAAJStreamWriter) responseContext.get("org.apache.cxf.staxutils.W3CDOMStreamWriter"));
        StringWriter stringWriter = new StringWriter();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        transformer.transform(new DOMSource(responseWriter.getDocument()), new StreamResult(stringWriter));
        return stringWriter.toString();
    }

/*
    protected static String validateSignedResponse(Map<String, Object> responseContext) throws TransformerException, WSSecurityException {
        WSSecurityEngineResult result = (WSSecurityEngineResult) responseContext.get("wss4j.signature.result");
        SAAJStreamWriter responseWriter = ((SAAJStreamWriter) responseContext.get("org.apache.cxf.staxutils.W3CDOMStreamWriter"));
       result.
        Element element = responseWriter.getDocument().getDocumentElement();
        List<WSSecurityEngineResult> resultList = new ArrayList<WSSecurityEngineResult>();
        resultList.add(result);
 */
/*       SignatureUtils.verifySignedElement(element, resultList);
*//*

        return "";
    }
*/

}
