rm clientKS.jks
rm serviceKS.jks

rm -rf demoCA
mkdir -p demoCA/newcerts

touch demoCA/index.txt
echo '01' > demoCA/serial
echo 'unique_subject = no' > demoCA/index.txt.attr

echo ----------------------------------------------
echo Creating CA
#Generate Key
openssl genrsa -out client.key 2048
#Generate Certificate based in key
openssl req -new -sha256 -x509 -days 3650 -key client.key -out client.crt -subj '/CN=client' -config openssl.conf

echo ----------------------------------------------
echo Create Certificate Signing Request
openssl req -config openssl.conf \
      -key client.key \
      -subj '/CN=client' \
      -new -sha256 -out client.csr

echo ----------------------------------------------
echo Sign Certificate
openssl ca -config openssl.conf \
      -batch \
      -extensions usr_cert \
      -days 3650 -notext -md sha256 \
      -keyfile client.key \
      -cert client.crt \
      -policy policy_anything \
      -in client.csr \
      -out client.pem

echo ----------------------------------------------
echo Verify Certificate
openssl x509 -noout -text \
      -in client.crt

echo ----------------------------------------------
echo Verify Chain
openssl verify -CAfile client.crt \
      client.crt


openssl pkcs12 -export -out client.p12 -inkey client.key -in client.crt -passout pass:password
#Exporting to tmp jks with default alias "1" since it is not possible with keytool to specify alias "client"
keytool -importkeystore -srckeystore client.p12 -srcstoretype PKCS12 -srcstorepass password -deststorepass client -destkeypass password -destkeystore clientTemp.jks -noprompt
#Importing to final jks with "client" alias
keytool -importkeystore -srckeystore clientTemp.jks -srcstoretype JKS -srcstorepass client -srckeypass password -alias 1 -destalias client -srcalias 1 -deststorepass client -destkeypass password -destkeystore clientKS.jks -noprompt

keytool -delete -keystore /home/tech/work/NATIONAL_GRID/servers/wso2ei-6.1.1/repository/resources/security/wso2carbon.jks -storepass wso2carbon -alias client
keytool -import -trustcacerts -keystore /home/tech/work/NATIONAL_GRID/servers/wso2ei-6.1.1/repository/resources/security/wso2carbon.jks -storepass wso2carbon -alias client -file client.crt -noprompt
keytool -delete -keystore /home/tech/work/NATIONAL_GRID/servers/wso2ei-6.1.1/repository/resources/security/wso2carbon.jks -storepass wso2carbon -alias rootCA
keytool -import -trustcacerts -keystore /home/tech/work/NATIONAL_GRID/servers/wso2ei-6.1.1/repository/resources/security/wso2carbon.jks -storepass wso2carbon -alias rootCA -file rootCA.crt -noprompt

keytool -export -keystore /home/tech/work/NATIONAL_GRID/servers/wso2ei-6.1.1/repository/resources/security/wso2carbon.jks -storepass wso2carbon -alias wso2carbon -file service.cer
keytool -import -trustcacerts -keystore clientKS.jks -storepass client -alias wso2carbon -file service.cer -noprompt

rm clientTemp.jks
rm client.*
rm -rf demoCA
