package com.chakray.security.ws;

import org.apache.ws.security.WSPasswordCallback;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;

public class PasswordCallbackHandler implements CallbackHandler {

    public void handle(Callback[] callbacks) throws IOException,
            UnsupportedCallbackException {
        for (int i = 0; i < callbacks.length; i++) {
            WSPasswordCallback pwcb = (WSPasswordCallback) callbacks[i];
            String id = pwcb.getIdentifier();
            int usage = pwcb.getUsage();

            System.out.println("Identifier2: " + id);
            System.out.println("Usage: " + usage);
            System.out.println("getKeyType: " + pwcb.getKeyType());
            System.out.println("getKey: " + pwcb.getKey());
            System.out.println("getCustomToken: " + pwcb.getCustomToken());

            if (usage == WSPasswordCallback.USERNAME_TOKEN) {
                // Logic to get the password to build the username token
                if ("admin".equals(id)) {
                    pwcb.setPassword("admin");
                }
            } else if (usage == WSPasswordCallback.SIGNATURE || usage == WSPasswordCallback.DECRYPT || usage == WSPasswordCallback.KEY_NAME) {
                // Logic to get the private key password for signature or decryption
                if ("wso2carbon".equals(id)) {
                    pwcb.setPassword("wso2carbon");
                }
            }
            pwcb.setPassword("wso2carbon");
        }

    }
}
