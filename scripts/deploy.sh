WSO2EI_PRODUCT=wso2ei-6.1.1
WORK=/home/tech/work/NATIONAL_GRID/git
SERVERS=/home/tech/work/NATIONAL_GRID/servers

mvn -f $WORK/projects/test/pom.xml clean install
mvn -f $WORK/projects/testREG/pom.xml clean install
mvn -f $WORK/projects/testCAR/pom.xml clean install
mv $WORK/projects/testCAR/target/*.car $SERVERS/$WSO2EI_PRODUCT/repository/deployment/server/carbonapps/

mvn -f $WORK/projects/RampartPasswordCallbackHandler/pom.xml clean install
mv $WORK/projects/RampartPasswordCallbackHandler/target/*.jar $SERVERS/$WSO2EI_PRODUCT/lib

#sed -i -e "s|<phase name="MsgOutObservation">.*</phase>|<phase name="MsgOutObservation"><handler name="CustomSecurityHandler" class="com.chakray.axis2.customhanlder.CustomSecurityHandler"/></phase>|g"  ../servers/$WSO2EI_PRODUCT/conf/axis2/axis2.xml


